<?php
/*Variables enviadas por el mikrotik*/
   $mac = $_POST['mac'];
   $local = $_POST['local'];
   $ip = $_POST['ip'];
   $username = $_POST['username'];
   $linklogin = $_POST['link-login'];
   $linkorig = $_POST['link-orig'];
   $error = $_POST['error'];
   $chapid = $_POST['chap-id'];
   $chapchallenge = $_POST['chap-challenge'];
   $linkloginonly = $_POST['link-login-only'];
   $linkorigesc = $_POST['link-orig-esc'];
   $macesc = $_POST['mac-esc'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Fridays</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="expires" content="-1" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="description" content="Friday’s Panama Zona Wifi, te permite ingresar a nuestra zona Wifi Free en cualquiera de tus dispositivos." />
	    <meta name="keywords" content="Fridays, Panama, zona wifi, comida, facebook, instagram" />
	    <meta name="author" content="Fidku Technology Inc.">
	    <meta name="robots" content="NOINDEX">
	    <meta name="revisit-after" content="2 days">
	    <meta property="og:url" content="https://fridays.com.pa" />
	    <meta property="og:type" content="website" />
	    <meta property="og:locale" content="es_PA" />
	    <meta property="og:title" content="Zona Wifi | Friday’s Panamá" />
	    <meta property="og:description" content="Friday’s Panama Zona Wifi, te permite ingresar a nuestra zona Wifi Free en cualquiera de tus dispositivos." />
	    <meta property="og:image" content="https://fridays.com.pa/assets/fd_cover.png" />
	    <meta name="twitter:card" content="summary_large_image">
	    <meta name="twitter:site" content="@FridaysPanama">
	    <meta name="twitter:creator" content="@FridaysPanama">
	    <meta name="twitter:title" content="Zona Wifi | Friday’s Panamá">
	    <meta name="twitter:description" content="Friday’s Panama Zona Wifi, te permite ingresar a nuestra zona Wifi Free en cualquiera de tus dispositivos.">
	    <meta name="twitter:image" content="https://fridays.com.pa/assets/fd_cover.png">

	    <link rel="shortcut icon" href="img/facivon.png">
    	<link rel="stylesheet" href="css/fonts.css" media="all" />
    	<link rel="stylesheet" href="css/style.css" media="all" />
    	<link rel="stylesheet" href="css/toast.min.css" media="all" />
    	<link rel="stylesheet" href="css/font-awesome.min.css" media="all" />

  		<script type="text/javascript" src="./jquery.js"></script>
  		<script type="text/javascript" src="./toast.min.js"></script>
  		<script type="text/javascript" src="./jquery.mask.min.js"></script>

  		<!--Almacenando variables enviadas por el mikrotik localmente para los casos en que el usuario tiene que salir de la pagina de registro y regresar.-->
  		<?php if(!empty($linkorig) && !empty($linkloginonly)){?>
	  		<script type="text/javascript">
	  			sessionStorage.setItem('ip', '<?php echo $ip; ?>');
	  			sessionStorage.setItem('username', '<?php echo $username; ?>');
	  			sessionStorage.setItem('linkorig', '<?php echo $linkorig; ?>');
	  			sessionStorage.setItem('linkloginonly', '<?php echo $linkloginonly; ?>');
	  			sessionStorage.setItem('chapid', '<?php echo $chapid; ?>');
	  			sessionStorage.setItem('chapchallenge', '<?php echo $chapchallenge; ?>');
	  			sessionStorage.setItem('local', '<?php echo $local; ?>');
	  			sessionStorage.setItem('mac', '<?php echo $mac; ?>');
	  		</script>
  		<?php } ?>

  		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-31728854-9"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-31728854-9');
		</script>
	</head>

	<body>
		<noscript>
		    <h2>Debes habilitar javascript para ver esta pagina.</h2>
		</noscript>
		<!--Formulario para logeo en mikrotik https://wiki.mikrotik.com/wiki/HotSpot_external_login_page-->
		<div style="display: none;">
			<form name="sendin" id="login1" action="<?php echo $linkloginonly; ?>" method="post">
				<input type="hidden" name="username" />
				<input type="hidden" name="password" id="passw" />
				<input type="hidden" name="dst" id="dst" value="<?php echo $linkorig; ?>" />
				<input type="hidden" name="popup" value="true" />
			</form>

			<script type="text/javascript" src="./md5.js"></script>
			<script type="text/javascript">
			    function doLogin() {
			    	var ch = sessionStorage.getItem('chapid');
			    	if(ch && ch.length < 1 ) return true;
			    	d = sessionStorage.getItem('linkorig'),
					c = sessionStorage.getItem('chapid'),
					cc = sessionStorage.getItem('chapchallenge');
			        <?php //if(strlen($chapid) < 1) echo "return true;\n"; ?>
					document.sendin.username.value = document.login.username.value;
					document.sendin.password.value = hexMD5(c + '123' + cc);
					document.sendin.submit();
					return false;
			    }
			</script>
		</div>

		<div class="App">
			<div class="loader" id="loader">
				<div class="spin">
					<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i>
				</div>
			</div>
			<!--Caja de terminos y condiciones -->
			<div class="terms_conditions" id="terms-content">
				<h2 class="title">T&eacute;rminos y condiciones del HOT SPOT FRIDAYS PANAMA</h2>
				
				<p class="txt">Condiciones de uso de la Red WIFI</p>

				<p class="txt">
					Al acceder y utilizar la red WIFI de los restaurantes Fridays, usted declara que ha leído, entendido y acepta los términos y condiciones para su utilización. Si usted no está de acuerdo con esta norma, no debe utilizar este servicio.
				</p>

				<p class="txt">
					Usted acepta y reconoce que hay riesgos potenciales a través de un servicio WIFI.
				</p>

				<p class="txt">
					Debe tener cuidado al transmitir datos como: número de tarjeta de crédito, cuentas bancarias, contraseñas u otra información personal sensible a través de redes WIFI. Los restaurantes Fridays no pueden y no garantizan la privacidad y seguridad de sus datos y de las comunicaciones al utilizar este servicio. 
				</p>

				<p class="txt">
					No garantizamos el nivel de desempeño de la red WIFI. El servicio puede no estar disponible o ser limitado en cualquier momento y por cualquier motivo, incluyendo emergencias, fallo del enlace, problemas en equipos de red, interferencias o fuerza de la señal. El restaurante Fridays, no se responsabiliza por datos, mensajes o páginas perdidas, no guardadas o retrasos por interrupciones o problemas de rendimiento con el servicio.
				</p>

				<p class="txt">
					NO se podrá utilizar la red WIFI para transmitir, copiar y/o descargar cualquier material que viole cualquier ley, esto incluye entre otros: material con derecho de autor, pornografía infantil, material amenazante u obsceno.
				</p>
				<div>
					<div class="btn-term btn-general" id="acept-term">
						<spam>ACEPTAR</spam>
					</div>
				</div>
			</div>
			<!--Fin caja de terminos y condiciones -->
			<div class="header txt-center">
				<h2 class="header-title txt-color-w"> Bienvenidos </h2>
				<img src="img/Logo.png" alt="Logo" />
			</div>
			<div class="principal">
				<div class="txt-center img-wifi ImageWifi">
					<img src="img/wifi.png" alt="wifi" />
				</div>

				<div class="txt-center box-connect txt-color-w BoxConnect">
					<h3 class="title-1"> para conectarse a la red </h3>
					<h3 class="title-2"> ingresar con: </h3>

					<div data-action="facebook" class="button btn-face btn-register">
						<div class="btn-symbol">
							<img src="img/facebook.png" alt="boton" class="txt-img-login" />
						</div>
						<span class="txt-btn-login"> ingresar por facebook </span>
					</div>

					<div data-action="instagram" class="button btn-instagram btn-register">
						<div class="btn-symbol">
							<img src="img/instagram.png" alt="boton" class="txt-img-login" />
						</div>
						<span class="txt-btn-login"> ingresar por instagram </span>
					</div>

					<div data-action="email" class="button btn-mail btn-register">
						<div class="btn-symbol">
							<img src="img/mail.png" alt="boton" class="txt-img-login" />
						</div>
						<span class="txt-btn-login"> ingresar por email </span>
					</div>

					<div class="terms">
						<input type="checkbox" id="terms" value="accept_terms" class="check" />
						<label for="terms" class="label-check"></label>
						<a href="#" class="link txt-color-w" id="show-terms"> aceptar t&eacute;rminos y condiciones </a>
					</div>
				</div>

				<div class="txt-center box-email box-connect txt-color-w BoxEmail" style="display: none;">
					<h3 class="title-1 p-b-20 p-t-20"> para conectarse a la red </h3>
					<h3 class="title-2 p-b-20"> registra tus datos: </h3>

					<form id="registerEmail" name="registerEmail" class="form-email">
						<div class="input-group p-b-10">
							<div class="symbol"><i class="fa fa-user-o" aria-hidden="true"></i></div>
							<input type="text" id="name" name="name" placeholder="nombre" class="input" requiered="true" />
						</div>
						<div class="input-group p-b-10">
							<div class="symbol"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
							<input type="text" id="email" name="email" placeholder="email" class="input" requiered="true"/>
						</div>
						<div class="input-group p-b-10">
							<div class="symbol"><i class="fa fa-birthday-cake" aria-hidden="true"></i></div>
							<input type="text" id="date" name="date" placeholder="DD/MM/YYYY" class="input" requiered="true"/>
						</div>
						<div class="btn-email-content">
							<button type="submit" class="btn-reg-email txt-color-w" id="saveEmail"> ir a navegar :) </button>
						</div>
					</form>
					<div class="footer txt-center txt-color-w">
						<p> &copy; 2017 tgi fridays inc. all rights reserved </p>
					</div>
				</div>
				<div><p class="signuperror"><?php echo $error; ?></p></div>
				<div class="footer txt-center txt-color-w Footer">
					<p> &copy; 2017 tgi fridays inc. all rights reserved </p>
				</div>
			</div>
		</div>
		<!--Formulario para logeo en mikrotik https://wiki.mikrotik.com/wiki/HotSpot_external_login_page-->
		<div style="display: none;">
			<form name="login" id="login2" action="<?php echo $linkloginonly; ?>" method="post" onSubmit="return doLogin()">
				<input type="hidden" name="dst" id="dst2" value="<?php echo $linkorig; ?>" />
				<input type="hidden" name="popup" value="true" />
				<input name="username" id="um" type="hidden" value="<?php echo !empty($username) ? $username : 'admin'; ?>"/>
				<input name="password" id="pw" type="hidden" value="123" />
				<input type="submit" id="loginMikrotik" value="OK" />
			</form>
		</div>

		<script type="text/javascript">
			(function($){
				//recuperando variables almacenadas localmente necesarias para el login en mikrotik
				var l = sessionStorage.getItem('linkloginonly'),
					  d = sessionStorage.getItem('linkorig'),
					  c = sessionStorage.getItem('chapid'),
					  cc = sessionStorage.getItem('chapchallenge'),
					  localF = sessionStorage.getItem('local'),
	  				  mac = sessionStorage.getItem('mac');
	  			//asignando valores a campos de formulario para login en mikrotik
				$('#login1').attr('action',l ? l : '');
				$('#login2').attr('action',l ? l : '');
				$('#dst').val(d ? d : '');
				$('#dst2').val(d ? d : '');
				$('#passw').val(hexMD5(c + '123' + cc));

				//metodo para validar fecha escrita por el usuario
				var optionsDate =  {
					onComplete: function(date) {
						var ard = date.split('/');
						if(Array.isArray(ard)){
							if(ard[0] <= 0 || ard[0] > 31){
								showMessage('Fecha no válida.','de2637');
								$('#date').val('');
							}
							if(ard[1] <= 0 || ard[1] > 12){
								showMessage('Fecha no válida.','de2637');
								$('#date').val('');
							}
							if(ard[2] <= 1920 || ard[2] > <?php echo (date('Y') - 10); ?>){
								showMessage('Fecha no válida.','de2637');
								$('#date').val('');
							}
						}else{
							showMessage('Formato de fecha no válido.','de2637');
							$('#date').val('');
						}
					}
				};
				//aplicando mascara al input de fecha
				$('#date').mask('00/00/0000',optionsDate);

				//settings de cabecera para request
				var settings = {
						"headers": {
							"content-type": "application/json"
						}
					};
				// configuraciones generales
				var config = {
						api: "http://fridays.lp.fudki.com:8099/",
						facebook_id: '141527443221429',
						facebook_url: 'http://fridays.lp.fudki.com',
						instagram_id: 'e901d534b5374dcf9d37d121f2487253',
						instagram_url: 'http://fridays.lp.fudki.com'
					};

				//en caso de login por facebook o instagram (devuelven un hash)
				var data = window.location.hash;
				if(data){
					data = data.split('=');
					if(data.length > 1){
						var load = document.getElementById('loader'),
							  typeR = sessionStorage.getItem('type_register');
		        		load.style.display = 'block';
		        		//Si el intento de login fue por instagram
		        		if(typeR == 'instagram'){
							data = data[1];
							settings.headers = '';
							settings.url = 'https://api.instagram.com/v1/users/self/?access_token='+data;
							settings.method = 'GET';

							$.ajax(settings)
							.done(function (response) {
								var datos = {
										email: response.data.username,
										name: response.data.full_name,
										local: localF,
										mac: mac,
										type: 'instagram'
									  };
								settings.headers = {"content-type": "application/json"};
								settings.url = config.api + 'register';
								settings.method = 'POST';
								settings.data = JSON.stringify(datos);

								$.ajax(settings)
								.done(function (response2) {
									load.style.display = 'none';
									if(response2.response == 'FAIL'){
										showMessage('No se pudo completar el registro.','de2637');
									}else{
										sessionStorage.setItem('email', response.data.username);
										limpiarCache();
										window.location.hash = '';
										$('#loginMikrotik').trigger('click');
									}
								})
								.fail(function(error) {
									load.style.display = 'none';
									showMessage('No se pudo completar el registro.','de2637');
								});
							})
							.fail(function(error) {
								load.style.display = 'none';
								showMessage('No se pudo completar el registro.','de2637');
							});
						}else{
							//Si el intento de login fue por facebook
							if(typeR == 'facebook'){
								var token = data[1].substr(0,data[1].indexOf('&'));

								settings.headers = {"Authorization": "Bearer "+token};
								settings.url = 'https://graph.facebook.com/me?fields=id,name,email,age_range,gender';
								settings.method = 'GET';

								$.ajax(settings)
								.done(function (response) {
									load.style.display = 'none';
									if(response){
										if(!response.name || !response.email){
											load.style.display = 'none';
											showMessage('Faltan datos para completar el registro.','de2637');
											return;
										}
										var min = 0,max = 0;

										if(response.age_range){
											if(response.age_range.min) min = response.age_range.min;
											if(response.age_range.max) max = response.age_range.max;
										}

										var datos = {
												email: response.email,
												name: response.name,
												age_min: min,
												age_max: max,
												gender: response.gender,
												local: localF,
												mac: mac,
												type: 'facebook'
											  };

										settings.headers = {"content-type": "application/json"};
										settings.url = config.api + 'register';
										settings.method = 'POST';
										settings.data = JSON.stringify(datos);

										$.ajax(settings)
										.done(function (response2) {
											load.style.display = 'none';
											if(response2.response == 'FAIL'){
												showMessage('No se pudo completar el registro.','de2637');
											}else{
												window.location.hash = '';
												sessionStorage.setItem('email', response.email);
												limpiarCache();
												$('#loginMikrotik').trigger('click');
											}
										})
										.fail(function(error) {
											load.style.display = 'none';
											showMessage('No se pudo completar el registro.','de2637');
										});
									}else{
										load.style.display = 'none';
										showMessage('No se pudo completar el registro.','de2637');
									}
								})
								.fail(function(error) {
									load.style.display = 'none';
									showMessage('No se pudo completar el registro.','de2637');
								});
							}else{
								window.location.hash = '';
								load.style.display = 'none';
							}
						}
						sessionStorage.setItem('type_register', undefined);
					}
				}
				//Metodo para mostrar mensajes al usuario (https://github.com/kamranahmedse/jquery-toast-plugin)
				showMessage = function(msg,color){
					var toas = $.toast({ 
					  text : msg, 
					  showHideTransition : 'slide',
					  bgColor : '#'+color,
					  textColor : '#fff',
					  allowToastClose : true,
					  hideAfter : false,
					  stack : 1,
					  textAlign : 'left',
					  position : 'top-center'
					});
					setTimeout(function(){toas.close();},2000);
				}

				//Evento para el enlace de mostrar terminos y condiciones
				$('#show-terms').click(function(e){
					e.preventDefault();
					$('#terms-content').show();
				});

				//Evento para aceptar los terminos y condiciones
				$('#acept-term').click(function(e){
					$('#terms-content').hide();
					$('#terms').attr('checked', true);
				});

				//evento del boton de registro
				$('.btn-register').click(function(e){
					var type = $(e.currentTarget).data('action'),
						  check = $('#terms').prop('checked');
					//Verifica que se aceptaron los terminos y condiciones
					if(check){
						if(type == 'facebook'){
							sessionStorage.setItem('type_register', 'facebook');
							var urlF = 'https://www.facebook.com/dialog/oauth?client_id='+config.facebook_id+'&redirect_uri='+config.facebook_url+'&response_type=token&scope=email,public_profile';
							window.location = encodeURI(urlF);
						}
						if(type == 'instagram'){
							sessionStorage.setItem('type_register', 'instagram');
							window.location.href = 'https://api.instagram.com/oauth/authorize/?client_id='+config.instagram_id+'&redirect_uri='+config.instagram_url+'&response_type=token';
						}
						if(type == 'email'){
							$('.Footer').hide();
							$('.ImageWifi').hide();
							$('.BoxConnect').hide();
							$('.BoxEmail').show();
						}
					}else{
						showMessage('Debe aceptar los términos y condiciones.','de2637');
					}
				});
				//evento del boton regitro por email
				$('#saveEmail').click(function(e){
					event.preventDefault();
					var name = $('#name').val().trim(),
						email = $('#email').val().trim(),
						date = $('#date').val().trim();
					//validando data
					if(email == '' || name == '' || date == ''){
						showMessage('Debe completar todos los datos.','de2637');
						return;
					}

					var re = new RegExp('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$');
					if(!re.test(email.toLowerCase())){
						showMessage('Formato de email no válido.','de2637');
						return;
					}

					var load = document.getElementById('loader');
			        load.style.display = 'block';

					email = email.toLocaleLowerCase();

					var datos = {
							email: email,
							name: name,
							local: localF,
							mac: mac,
							type: 'email',
							birthday : date
						  };

					settings.headers = {"content-type": "application/json"};
					settings.url = config.api + 'register';
					settings.method = 'POST';
					settings.data = JSON.stringify(datos);
					$.ajax(settings)
					.done(function (response) {
						load.style.display = 'none';
						if(response.response == 'FAIL'){
							showMessage('No se pudo completar el registro.','de2637');
						}else{
							sessionStorage.setItem('email', email);
							limpiarCache();
							$('#loginMikrotik').trigger('click');
						}
					})
					.fail(function(error) {
						load.style.display = 'none';
						showMessage('No se pudo completar el registro.','de2637');
					});
				});
			})(jQuery);
		</script>
	</body>
</html>
