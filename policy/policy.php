<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE 8]>
<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>
<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>Políticas de Privacidad | Pulso</title>

    <link rel="shortcut icon" href="../assets/images/favicon.png"/>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="description" content="Políticas de Privacidad | Fridays">
    <meta name="author" content="Fidku Technology Inc.">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Theme Styles -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/components/owl-carousel/owl.carousel.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="../assets/components/owl-carousel/owl.transitions.css" media="screen"/>
    <!-- Magnific Popup core CSS file -->
    <link rel="stylesheet" href="../assets/components/magnific-popup/magnific-popup.css">

    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="../assets/css/style.css">

    <!-- Updated Styles -->
    <link rel="stylesheet" href="../assets/css/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="../assets/css/custom.css">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="../assets/css/responsive.css">

    <!-- CSS for IE -->
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="assets/css/ie.css"/>
    <![endif]-->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>
<body>
<div id="page-wrapper">
    <header id="header">
        <div class="container">
            <div class="header-inner">
                <div class="branding">
                    <h1 class="logo">
                        <a href="index.html"><img src="../assets/images/logo@2x.png" alt=""/></a>
                    </h1>
                </div>
                <nav id="nav">
                    <ul class="header-top-nav">

                        <li class="visible-mobile">
                            <a href="#mobile-nav-wrapper" data-toggle="collapse"><i
                                        class="fa fa-bars has-circle"></i></a>
                        </li>
                    </ul>
                    <ul id="main-nav" class="hidden-mobile">
                        <li class="menu-item-has-children">
                            <a href="index.html">Inicio</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="mobile-nav-wrapper collapse visible-mobile" id="mobile-nav-wrapper">
            <ul class="mobile-nav">
                <li class="menu-item-has-children">
                    <span class="open-subnav"></span>
                    <a href="index.html">Inicio</a>
                </li>
            </ul>
        </div>
    </header>

    <div class="page-title-container">
        <div class="page-title">
            <div class="container">
                <h1 class="entry-title">Políticas de Privacidad</h1>
            </div>
        </div>
        <ul class="breadcrumbs">
            <li class="active">21 de Diciembre de 2017</li>
        </ul>
    </div>

    <section id="content">
        <div class="container">
            <div class="row">
                <div class="sidebar col-sm-4 col-md-3">
                    <ul class="arrow-circle hover-effect filter-options">
                        <li class="active"><a href="#">Políticas de Privacidad</a></li>
                    </ul>
                </div>
                <div id="main" class="col-sm-8 col-md-9">
                    <div class="box">
                        <h3>Te damos la bienvenida a Friday's Panamá HostSpot</h3>
                        <p>Te agradecemos que uses nuestra página web de ingreso a Wifi(en adelante, el «Hostpot»). La Hostpot se
                            proporciona a través de Fidku Technology Inc. (en adelante, «Fidku»), cuyo domicilio está
                            ubicado en PH Torre Delta, Ciudad de Panamá, Panamá. Para el uso y mantención de el Hostpot
                            se necesita contar con información de cada uno de los usuarios que
                            hacen uso de el mismo, el siguiente documento establece el uso que se le da a la
                            información entregada por el usuario.</p>
                        <ul class="bullet-text decimal paragraph">
                            <li>El uso de nuestro Hostpot a través de Facebook, Instagram o Email, ingresando
                                a través de los links de referencia o en nuestro sitio web (http://fridays.lp.fudki.com),
                                implica la aceptación de las Políticas de Privacidad. Te recomendamos que las leas detenidamente.
                            </li>
                            <li>Si no estás de acuerdo con todas las Políticas aquí descritas, no debes hacer uso de el
                                Hostpot, bajo ninguna modalidad (Facebook, Instagram o Email)
                            </li>
                        </ul>
                    </div>
                    <div class="box">
                        <h3>Definición de Usuarios</h3>
                        <p>Se define como usuario a quién hace uso de nuestro Hostpot.</p>
                        <ul class="bullet-text disc">
                            <li>Los usuarios pueden ingresar utilizando Facebook, Instagram o Email para utilizar el Wifi del local.
                            </li>
                            <li>Los usuarios pueden ingresar a través de una url de referencia para ingresar directamente al Hostpot.
                            </li>
                            <li>Los usuarios pueden ingresar al sitio http://fridays.lp.fudki.com e iniciar sesión,
                                siempre y cuando esten dentro de la misma red del local.
                            </li>
                        </ul>
                    </div>
                    <div class="box">
                        <h3>¿Qué información se obtiene de los usuarios?</h3>
                        <div class="row">
                            <div class="col-md-6 box">
                                <div class="panel-group" id="accordion-1">
                                    <div class="panel style5">
                                        <h5 class="panel-title">
                                            <a href="#acc1-1" data-toggle="collapse" data-parent="#accordion-1">
                                                <span class="open-sub"></span>
                                                Identificador de Cuenta
                                            </a>
                                        </h5>
                                        <div class="panel-collapse collapse" id="acc1-1">
                                            <div class="panel-content">
                                                <p>Se genera un ID único (UUID) para identificar al usuario. En ningún
                                                    momento este ID es enviado a Facebook, compartido en Internet o entre servidores, así también,
                                                    el ID NO permite identificar al usuario con su cuenta de Facebook ni tampoco de Instagram.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel style5">
                                        <h5 class="panel-title">
                                            <a href="#acc5-2" data-toggle="collapse" data-parent="#accordion-5">
                                                <span class="open-sub"></span>
                                                Información Personal
                                            </a>
                                        </h5>
                                        <div class="panel-collapse collapse" id="acc5-2">
                                            <div class="panel-content">
                                                <p>Al comenzar la conversación se obtiene el nombre del usuario, email y rango de edad,
                                                    sólo con la finalidad de personalizar la experiencia en el wifi del local.
                                                    La información es guardada, para no volver a solicitar registro la
                                                    próxima vez que ingresal al HostPot.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <h3>¿El Hostpot obtiene información precisa y en tiempo real del dispositivo?</h3>
                        <p>NO. En ningún caso se solicita la ubicación del usuario.</p>
                    </div>
                    <div class="box">
                        <h3>¿Pueden terceros obtener acceso a la información obtenida por el HostPot?</h3>
                        <p>Si. Podemos compartir la información proporcionada por el usuario en los siguientes casos:</p>
                        <ul class="bullet-text disc">
                            <li>Cuando sea requerido por la ley.</li>
                            <li>Cuando creamos que es necesario para proteger nuestros derechos, proteger su seguridad o
                                la seguridad de otros, investigar fraude o en respuesta a una solicitud del gobierno.
                            </li>
                            <li>Para realizar análisis de la información (BigData) a través de proveedores como
                                Amazon y para el mejoramiento del Hostpots.
                            </li>
                        </ul>
                    </div>
                    <div class="box">
                        <h3>Recopilación Automática de datos</h3>
                        <p>El Hostpot utiliza medios de analisis de información en línea, tales como Google Analytics
                            y Facebook Analytics con el fin de entender el uso, tiempo y frecuencia que los usuarios hacen
                            uso del HostPot.</p>

                    </div>
                    <div class="box">
                        <h3>Administración de los datos</h3>
                        <p>Conservaremos los datos proveídos por el usuario y los recolectados automáticamente de forma
                            indefinida y totalmente ANÓNIMA, es imposible linquear o atribuir registros e ingresos a un usuario
                            en específico.</p>
                    </div>
                    <div class="box">
                        <h3>Más información</h3>
                        Para obtener más información del cómo, qué y cuando se utiliza la información, por favor
                        envíenos un email a hi@fidku.com, adjuntando sus dudas.
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer id="footer" class="style4">
        <div class="footer-bottom-area">
            <div class="container">
                <div class="copyright-area">
                    <div class="copyright">
                        &copy; 2018 <em>by</em> <a href="http://www.fidku.com/">Fidku Technology Inc.</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<!-- Javascript -->
<script type="text/javascript" src="../assets/js/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery.noconflict.js"></script>
<script type="text/javascript" src="../assets/js/modernizr.2.8.3.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery-ui.1.11.2.min.js"></script>

<!-- Twitter Bootstrap -->
<script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>

<!-- Magnific Popup core JS file -->
<script type="text/javascript" src="../assets/components/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- parallax -->
<script type="text/javascript" src="../assets/js/jquery.stellar.min.js"></script>

<!-- waypoint -->
<script type="text/javascript" src="../assets/js/waypoints.min.js"></script>

<!-- Owl Carousel -->
<script type="text/javascript" src="../assets/components/owl-carousel/owl.carousel.min.js"></script>

<!-- plugins -->
<script type="text/javascript" src="../assets/js/jquery.plugins.js"></script>

<!-- load page Javascript -->
<script type="text/javascript" src="../assets/js/main.js"></script>
</html>